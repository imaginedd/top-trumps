<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php $terms = get_the_terms($post->id, 'card_category'); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'card_container color-palette ' . get_field('card_bg_colour') . ' ' . $terms[0]->slug ); ?> >
	
	<section class="card_image">
			<?php the_post_thumbnail('card__img_rect'); ?>
			<header>
				<?php the_title( '<h2 class="entry-title">', '</a></h2>' ); ?>
			</header><!-- .entry-header -->
	</section>

	<div class="entry-content">

	<table>
		<tr>
			<th><?php _e('Flour', 'toptrumps'); ?></th>
			<td class="flour"><?php
			/* translators: %s: Card meta Volume of Flour g: shorthand measurement grams */
			printf(
				__('%sg', 'toptrumps'),
				get_field('card_meta__flour_weight')
			);
			?></td>
		</tr>
		<tr>
			<th><?php _e('Prep Time', 'toptrumps'); ?></th>
			<td class="preptime"><?php
			/* translators: %s: Card meta Preparation Time mins: shorthand minutes */
			printf(
				__('%s mins', 'toptrumps'),
				get_field('card_meta__prep_time')
			);
			?></td>
		</tr>
		<tr>
			<th><?php _e('Cooking Time', 'toptrumps'); ?></th>
			<td class="cookingtime"><?php
			/* translators: %s: Card meta Required cooking time mins: shorthand minutes */
			printf(
				__('%s mins', 'toptrumps'),
				get_field('card_meta__cooking_time')
			);
			?></td>
		</tr>
		<tr>
			<th><?php _e('Oven Temp', 'toptrumps'); ?></th>
			<td class="oventemp"><?php
			/* translators: %s: Card meta Oven cooking temperature &deg;C: shorthand degrees centigrade */
			printf(
				__('%s &deg;C', 'toptrumps'),
				get_field('card_meta__oven_temperature')
			);
			?></td>
		</tr>
		<tr>
			<th><?php _e('Servings', 'toptrumps'); ?></th>
			<td class="servings"><?php
			/* translators: %s: Card meta Number of servings */
			printf(
				__('%s', 'toptrumps'),
				get_field('card_meta__servings')
			);
			?></td>
		</tr>
		<tr>
			<th><?php _e('Tastiness', 'toptrumps'); ?></th>
			<td class="tastiness"><?php
			/* translators: %s: Card meta Taste value */
			printf(
				__('%s', 'toptrumps'),
				get_field('card_meta__tastiness')
			);
			?></td>
		</tr>

	</table>

	</div><!-- .entry-content -->

</article><!-- #post-## -->