<?php
/**
 * Twenty Sixteen Child theme
 */

/*******************************************************
THEME: REGISTER STYLES
*******************************************************/

// Call in parent stylesheet
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles');
function enqueue_parent_styles(){
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}


add_action( 'wp_enqueue_scripts', 'enqueue_sort_js');
function enqueue_sort_js(){
	
	/*
	 * If archive page then bring in sort library
	 */
	if( is_archive() ) :

		wp_register_script( 'tinySortJS', get_stylesheet_directory_uri() . '/js/tinysort.min.js', array(), '2.3.6', false );
		wp_enqueue_script( 'tinySortJS' );

	endif;

	/*
	 * All custom JS contained within a single file.
	 */
	wp_register_script( 'trumpJS', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery', 'tinySortJS' ), '', true );
	wp_enqueue_script( 'trumpJS' );

}

/*******************************************************
THEME: REGISTER FONTS
*******************************************************/

// Call in Google Font relevant to each deck
add_action('wp_enqueue_scripts', 'required_deck_fonts');
function required_deck_fonts(){
	// Only call if on archive page
	if( is_archive() || is_single() ) :

		// Get current archive custom taxonomy
		global $post;
		$archive_terms = get_the_terms( $post->ID, 'card_category');

		if( $archive_terms ) :

			// For each Deck, set any custom fonts as per the Google embed code
			switch ( $archive_terms[0]->slug ){
				case "grandmas-best-recipes" :
					$fonts = "Great+Vibes";
					break;
				default :
					$fonts = "";
			}

		endif;

		// If fonts not empty, enqueue relevant Google Fonts for required Deck
		if( isset( $fonts ) ) :
			wp_enqueue_style('deckFonts', '//fonts.googleapis.com/css?family=' . $fonts);
		endif;

	endif;
}

/*******************************************************
DECK: REGISTER CUSTOM POST TYPE
*******************************************************/

// Create a new custom post type. Based on function from Bones Theme https://github.com/eddiemachado/bones/.
function add_custom_post_type() { 
	// creating (registering) the custom type
	register_post_type( 'trump_cards', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Top Trumps', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Card', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Cards', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add Card', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Card', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Cards', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Card', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Card', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Top Trumps Cards', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No Top Trumps Cards found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'No Top Trumps Cards found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the custom post type for Top Trumps Cards', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-index-card', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'cards', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'cards', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'thumbnail' )
		) /* end of options */
	); /* end of register post type */
}
// adding the function to the Wordpress init
add_action( 'init', 'add_custom_post_type');

// Add in a custom taxonomy to keep categories isolated for CPT
register_taxonomy( 'card_category', 
	array('trump_cards'), /* change this to same as the name of CPT */
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Card Decks', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Deck', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Decks', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Decks', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Deck', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Deck:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Deck', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Deck', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Deck', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Deck Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'deck' ),
	)
);

/*******************************************************
DECK: CUSTOM FIELDS
*******************************************************/

/*
 * Custom Fields created through Advanced Custom Fields plugin.
 * ACF required: https://en-gb.wordpress.org/plugins/advanced-custom-fields/
 * Chosen to keep custom fields in PHP over DB for easy visibility as well as easy alterations for future decks
 */

/*
 * Grandmas Best Recipes Card Meta
 */
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_grandmas-best-recipes-meta',
		'title' => 'Grandmas Best Recipes Meta',
		'fields' => array (
			array (
				'key' => 'field_57a9ebd5c1820',
				'label' => 'Flour (g)',
				'name' => 'card_meta__flour_weight',
				'type' => 'number',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_57a9eb6bc181e',
				'label' => 'Prep Time',
				'name' => 'card_meta__prep_time',
				'type' => 'number',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_57a9eb35c181d',
				'label' => 'Cooking Time',
				'name' => 'card_meta__cooking_time',
				'type' => 'number',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_57a9eb8bc181f',
				'label' => 'Oven Temperature',
				'name' => 'card_meta__oven_temperature',
				'type' => 'number',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_57a9ea99ddb77',
				'label' => 'Makes (Qty)',
				'name' => 'card_meta__servings',
				'type' => 'number',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_85f1fa42ead12',
				'label' => 'Tastiness',
				'name' => 'card_meta__tastiness',
				'type' => 'number',
				'required' => 1,
				'instructions' => 'On a scale of 0 (vom) to 10,000 (nom)',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trump_cards',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => '2',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 10,
	));
}

/*
 * Grandmas Best Recipes Card Background Colours
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_deck-colours',
		'title' => 'Deck Colours',
		'fields' => array (
			array (
				'key' => 'field_57ab0412679e4',
				'label' => 'Card Colour',
				'name' => 'card_bg_colour',
				'type' => 'radio',
				'choices' => array (
					'pink' => 'Pink',
					'purple' => 'Purple',
					'mint' => 'Mint Green',
					'lblue' => 'Light Blue',
					'dblue' => 'Dark Blue',
					'orange' => 'Orange',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trump_cards',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => '2',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 20,
	));
}

/*
 * Category image for each deck
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_category-image',
		'title' => 'Category Image',
		'fields' => array (
			array (
				'key' => 'field_57ab2b5b1ff1a',
				'label' => 'Category thumbnail',
				'name' => 'category_thumbnail',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'card_category',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}




/*******************************************************
THEME: IMAGE SIZES
*******************************************************/

// Create new image size to be used for deck taxonomy: card_category
add_image_size( 'card__img_rect', 850, 550, true ); //850w, 550h, true = cropped
add_image_size( 'deck__category_img', 250, 330, true ); //250w, 330h, true = cropped

// Overwrite pagination count for deck pages
add_action( 'pre_get_posts', 'deck_pagesize', 1 );
function deck_pagesize( $query ) {
	// If main query and on archive page and custom taxonomy is for Top Trumps card 
	if ( ( ! is_admin() ) && ( $query->is_main_query() ) && ( is_archive() ) && ( $query->tax_query->queries[0]['taxonomy'] == "card_category" ) ){
		// Display all cards on page without pagination
		$query->set( 'posts_per_page', -1 );
		return;
	}
}