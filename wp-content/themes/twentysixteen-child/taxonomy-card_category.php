<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="deck_header">
				<div class="deck_image">
					<?php
						$image = get_field('category_thumbnail', 'card_category_2');
						$size = 'deck__category_img';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size );
						}
					?>
				</div>
				<div class="deck_descr">
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</div>
			</header><!-- .page-header -->

			<section class="sort_menu">

				<p>You can re-organise this deck by selecting which criteria to order by, as well which direction to order in</p> 

				<select onChange="changeSortingOrder(this)">
					<option value="asc">Sort: Ascending Order</option>
					<option value="desc">Sort: Descending Order</option>
				</select>

				<select onChange="changeSortingFilter(this)">
					<option value="flour">Order by: Flour Quantity</option>
					<option value="preptime">Order by: Prep Time</option>
					<option value="cookingtime">Order by: Cooking Time</option>
					<option value="oventemp">Order by: Oven Temp</option>
					<option value="servings">Order by: Servings</option>
					<option value="tastiness">Order by: Tastiness</option>
				</select>

			</section>

			<section class="deck_catalogue">

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				// Get current taxonomy details
				$terms = get_the_terms($post->id, 'card_category');

				/*
				 * Overwritten this to call in specific deck content based on taxonomy slug
				 * Allows for customisation per deck as each deck will have custom meta
				*/ 
				get_template_part( 'template-parts/content', $terms[0]->slug );

			// End the loop.
			endwhile;

			?>

			</section>

		<?php

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
