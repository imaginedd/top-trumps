/*
 * Set default sorting criteria
 */

var sortingFilter = 'flour';
tinysort.defaults.order = 'asc';

/*
 * Sort cards using tinysort library.
 * argument 1: sets container to sort
 * argument 2: sets sorting criteria to sort by.
 */
function sortFilter() {
	tinysort(".deck_catalogue article.card_container","td." + sortingFilter);
}

/*
 * Change sorting filter and re-sort
 */
function changeSortingFilter(changeSortingFilter){
	sortingFilter = changeSortingFilter.value;
	sortFilter();
}

/*
 * Change order direction and re-sort
 */
function changeSortingOrder(changeSortingOrder){
	tinysort.defaults.order = changeSortingOrder.value;
	sortFilter();
}

/*
 * When page loaded, sort by default orting criteria
 */
jQuery(document).ready(function($) {
	sortFilter();
});